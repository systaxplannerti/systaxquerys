--use DB482265
drop table #tmpRETORNO_ISS;
drop table #tmpUPDATE_ISS;
drop table #tmpRETORNO_ISS_duplicado;
TRUNCATE table tbl_vertex_regra_iss
 

select 
			CS.cse_codi AS  CSE_CODI,		
			'SYSTAX_' + replace(ibge.ibge_desc, ' ', '_' ) + '_' + UF_PRESTADOR + '_' + 'ALL_ACTIVE_ISS_TAX_RATES' AS NOMEARQUIVO,
			CAST('SERV: ' + RIGHT('0' + COD_SUBITEM_LEI, 5) AS VARCHAR(100))  as RULE_DESCRIPTION,			
			CAST(CAST(   REPLACE( REPLACE(ISS_ALIQ , ' ', '0')  ,',','.') AS DECIMAL(18,2)) AS VARCHAR(30)) + '%' as IIS_ALIQ_FORMAT,			
			RIGHT('0' + COD_SUBITEM_LEI, 5) AS NCM_SERVICE_CODE,			
			'' AS CESTCode,
			'' AS CEST_Description,
CASE
	WHEN UF_PRESTADOR <> 'EX'
		THEN 'Domestic'
        ELSE 'Imported'
        END AS MATERIAL_ORIGIN,

CASE 
	WHEN ISS_GENE LIKE ('%ISSRet=S%') OR ISS_GENE LIKE ('%ISSRet=D%')
		THEN 'Acquisition'
		ELSE 'Sale' 
		END AS TRANSACTION_TYPE,
CASE
		WHEN ISS_GENE LIKE ('%ISSRet=D%')
		THEN 'inTERcity'
		ELSE 'Not Relevant'				
	END AS TRANSACTION_GEO,

	'BRAZIL' AS  O_COUNTRY,
	'City' AS  O_JURISDICTION_TYPE,
	UF_PRESTADOR AS PRESTADOR_UF, 
CASE
		WHEN ISS_GENE LIKE ('%ISSRet=D%')
		THEN 'All States'
		ELSE  UF_PRESTADOR	
	END AS O_STATE,  

CASE
		WHEN ISS_GENE LIKE ('%ISSRet=S%') OR ISS_GENE LIKE ('%ISSRet=D%')
		THEN 'All Cities'
		ELSE  ibge.ibge_desc	
	END AS O_JURISDICTION_NAME,  
	ibge.ibge_desc AS  MUNICIPIO_PRESTADOR,
	ibge.ibge_codi AS  MUNICIPIO_PRESTADOR_COD_IBGE,

CASE
		WHEN ISS_GENE LIKE ('%ISSRet=S%') OR ISS_GENE LIKE ('%ISSRet=D%')
		THEN  cast('' as varcHAR(30))
		ELSE  ibge.ibge_codi	
	END AS IBGE_Origin,  
	
	'Origin - Service Provider' AS  O_SELLER_CONDITION,

	'City' AS  D_JURISDICTION_TYPE,

	CASE
		WHEN  ISS_GENE LIKE ('%ISSRet=D%') OR ISS_GENE LIKE ('%ISSRet=S%')
		THEN 'All States'
		ELSE  UF_PRESTADOR	
	END AS D_STATE, 	
	
	CASE
		WHEN ISS_GENE LIKE ('%ISSRet=D%') OR ISS_GENE LIKE ('%ISSRet=S%')
		THEN 'All Cities'
		ELSE  ibge.ibge_desc
	END AS D_JURISDICTION_NAME, 			

	CAST('' AS VARCHAR(30)) AS  IBGE_Destination,
	
CASE 
	WHEN ISS_RESPONSAVEL LIKE '%TOMADOR%'
		THEN 'Destination - End Consumer - B2B'
		ELSE 'Destination - End Consumer'
	END  AS  D_BUYER_CONDITION,

CASE 
	WHEN ISS_RESPONSAVEL LIKE '%TOMADOR%'
		THEN 'ISS WHT'
		ELSE 'ISS'
	END  AS  [IMPOSITION],


	CAST(CAST(REPLACE( REPLACE(ISS_ALIQ , ' ', '0')  ,',','.') AS DECIMAL(18,2)) AS VARCHAR(30)) + '%' AS  TAXRATE,
CASE 
	WHEN ISS_GENE LIKE ('%ISSRedBC%')
		THEN SUBSTRING(ISS_GENE,CHARINDEX('=',ISS_GENE)+1,LEN(ISS_GENE))
		ELSE '100.0000%'
		END AS  BASIS_FACTOR,

CASE 
	WHEN ISS_GENE LIKE ('%ISSAlqRed=1%')	
		THEN 'Reduced Rate'
		ELSE 'Taxable Standard'
		END AS  Reduced_Rate,

CASE 
	WHEN ISS_RESPONSAVEL = 'Prestador'
		THEN 'Seller'
		ELSE 'Buyer'
		END AS TAX_RESPONSIBILITY,

	'Destination' AS TAX_IN_FAVOR_OF,
FORMAT(ISS_VIG_DE, 'yyyyMMdd') AS  EFFDATE,
'99991231' AS  ENDDATE,
STUFF(replace(ISS_DISPLEG, ' ', ''),
	charindex('/', replace(ISS_DISPLEG, ' ', ''), 
		charindex('/',replace(ISS_DISPLEG, ' ', ''), 
			charindex('/',replace(ISS_DISPLEG, ' ', ''), 
				charindex('/',replace(ISS_DISPLEG, ' ', '')) +1) +1) + 1)
, 1 , '&'+ ibge.ibge_desc + '/')  AS  CITATION,
CAST ('' AS VARCHAR(30)) AS  Rules_Created_or_Updated_at,
CAST ('' AS VARCHAR(30)) AS  New_Changed_Indicator,
CAST ('' AS VARCHAR(30)) AS  Tax_Rule_ID,
 CAST ('' AS VARCHAR(30)) AS hash_regra
into #tmpRETORNO_ISS
 from tbl_cenario_servico cs (nolock)
inner join tbl_servico s (nolock) on cs.ser_codi = s.ser_codi
inner join tbl_cenario c (nolock) on cs.cenario_codi = c.cenario_codi
inner join tbl_imp_iss imp (nolock) on cs.cse_codi = imp.cse_codi and imp.imp_ativ = 1 and IMP_STATUS = 'A'
inner join tbl_iss iss (nolock) on imp.imp_codi = iss.imp_codi
left join tbl_ibge ibge (nolock) on c.MUNICIPIO_PRESTADOR = ibge.ibge_codi
where cs.cse_ativ = 1 and C.id_cenario = 'SE2800308' --AND COD_SUBITEM_LEI = '11.02' --AND cs.cse_codi = 629

--ALTERANDO RULE_DESCRIPTION COM O FROM E TO QUE FOI MAPEADO
update #tmpRETORNO_ISS set RULE_DESCRIPTION = RULE_DESCRIPTION + ' ' + IMPOSITION + ' '+ IIS_ALIQ_FORMAT + ' from ' + O_SELLER_CONDITION + ' to ' + D_BUYER_CONDITION
update #tmpRETORNO_ISS set IBGE_Destination = MUNICIPIO_PRESTADOR_COD_IBGE  where IBGE_Origin = ''
update #tmpRETORNO_ISS set TAX_IN_FAVOR_OF = 'Origin' where TAX_RESPONSIBILITY = 'Seller'
update #tmpRETORNO_ISS set TAX_IN_FAVOR_OF = 'Destination' where TAX_RESPONSIBILITY = 'Buyer'
update #tmpRETORNO_ISS set hash_regra = HASHBYTES('MD5', RULE_DESCRIPTION + NCM_SERVICE_CODE + MATERIAL_ORIGIN + TRANSACTION_TYPE + TRANSACTION_GEO + O_COUNTRY + D_JURISDICTION_TYPE + O_STATE + O_JURISDICTION_NAME + IBGE_Origin + O_SELLER_CONDITION + D_JURISDICTION_TYPE + D_STATE + D_JURISDICTION_NAME + IBGE_Destination + D_BUYER_CONDITION + IMPOSITION + TAXRATE + BASIS_FACTOR + Reduced_Rate + TAX_RESPONSIBILITY + TAX_IN_FAVOR_OF + EFFDATE + ENDDATE + CITATION)
update #tmpRETORNO_ISS set D_STATE = 'All States' where IBGE_Origin <> '' AND TRANSACTION_GEO = 'Not Relevant'
update #tmpRETORNO_ISS set D_STATE = PRESTADOR_UF WHERE IBGE_Origin = '' 
update #tmpRETORNO_ISS set D_JURISDICTION_NAME = 'All Cities' WHERE IBGE_Origin <> '' AND TRANSACTION_GEO = 'Not Relevant';
update #tmpRETORNO_ISS set D_JURISDICTION_NAME =  MUNICIPIO_PRESTADOR WHERE IBGE_Origin = '' 
update #tmpRETORNO_ISS  set CITATION =  STUFF(CITATION, CHARINDEX('Lein', CITATION) + 4, 1, '') where CITATION like '%Lein%'


--BUSCANDO REGRAS DE ISS QUE J� EXISTIA E CONTINUA IGUAL PARA ATUALIZAR O INDICADOR E RETIRAR DA TABELA DE APOIO
select VERTEX_REGRA_ISS_CODI, A.CSE_CODI into #tmpUPDATE_ISS from tbl_vertex_regra_iss a (nolock)
inner join #tmpRETORNO_ISS b (nolock) on a.cse_codi = b.cse_codi
where a.[hash] = b.hash_regra
--ATUALIZANDO INDICADOR DA REGRA QUE J� EXISTIA 
update u set u.New_Changed_Indicator = null from #tmpUPDATE_ISS t
inner join tbl_vertex_regra_iss u on t.VERTEX_REGRA_ISS_CODI = u.VERTEX_REGRA_ISS_CODI


--ADICIONANDO AS REGRAS QUE IR�O DUPLICAR
select * into #tmpRETORNO_ISS_duplicado 
from #tmpRETORNO_ISS
where TRANSACTION_GEO = 'inTERcity'
--APLICANDO AS REGRAS
update #tmpRETORNO_ISS_duplicado set TRANSACTION_GEO = 'inTRAcity' 
UPdate #tmpRETORNO_ISS_duplicado set O_STATE = PRESTADOR_UF
UPdate #tmpRETORNO_ISS_duplicado set D_STATE = PRESTADOR_UF
UPdate #tmpRETORNO_ISS_duplicado set D_JURISDICTION_NAME = MUNICIPIO_PRESTADOR
UPdate #tmpRETORNO_ISS_duplicado set O_JURISDICTION_NAME = MUNICIPIO_PRESTADOR
UPdate #tmpRETORNO_ISS_duplicado set IBGE_Origin = MUNICIPIO_PRESTADOR_COD_IBGE 
UPdate #tmpRETORNO_ISS_duplicado set IBGE_Destination = '' where IBGE_Origin <> ''
UPdate #tmpRETORNO_ISS_duplicado set D_BUYER_CONDITION = 'Destination - End Consumer'
UPdate #tmpRETORNO_ISS_duplicado set IMPOSITION = 'ISS'
UPdate #tmpRETORNO_ISS_duplicado set TAX_RESPONSIBILITY = 'Seller'
UPdate #tmpRETORNO_ISS_duplicado set TAX_IN_FAVOR_OF = 'Origin'
UPdate #tmpRETORNO_ISS_duplicado set TRANSACTION_TYPE = 'Sale'
update #tmpRETORNO_ISS_duplicado set RULE_DESCRIPTION =	replace(RULE_DESCRIPTION, ' Destination - End Consumer - B2B',' Destination - End Consumer')
update #tmpRETORNO_ISS_duplicado set RULE_DESCRIPTION =	replace(RULE_DESCRIPTION, 'ISS WHT','ISS')
update #tmpRETORNO_ISS_duplicado set O_SELLER_CONDITION = 'Origin - Service Provider'
--update #tmpRETORNO_ISS_duplicado set CITATION =  REPLACE(charindex('Lein', CITATION), '?', '');
--update #tmpRETORNO_ISS_duplicado set CITATION =  STUFF(CITATION, CHARINDEX('Lein', CITATION) + 4, 1, '') where CITATION like '%Lein%'
--update #tmpRETORNO_ISS_duplicado set CITATION =  REPLACE('#', '', CITATION);
--update #tmpRETORNO_ISS_duplicado set CITATION =  REPLACE('$', '', CITATION);
--update #tmpRETORNO_ISS_duplicado set CITATION =  REPLACE('%', '', CITATION);
--update #tmpRETORNO_ISS_duplicado set CITATION =  REPLACE('¨', '', CITATION);
--update #tmpRETORNO_ISS_duplicado set CITATION =  REPLACE('*', '', CITATION);


--EXCLUINDO REGRA ISS QUE J� EXISTE E EST� IGUAL AO TABEL�O DE RETORNO
delete b from #tmpUPDATE_ISS a
inner join #tmpRETORNO_ISS b on a.CSE_CODI = b.CSE_CODI


--EXCLUINDO REGRA ISS DO TABEL�O, POIS A ATUAL � DIFERENTE
delete a from tbl_vertex_regra_iss a
inner join #tmpRETORNO_ISS b on a.cse_codi = b.cse_codi
where a.[hash] != b.hash_regra

--INSERINDO RESTANTE NO TABEL�O DE RETORNO
INSERT INTO tbl_vertex_regra_iss (CSE_CODI,RULE_DESCRIPTION,NCM_SERVICE_CODE,CESTCode,CEST_Description,MATERIAL_ORIGIN,TRANSACTION_TYPE,TRANSACTION_GEO,
O_COUNTRY,O_JURISDICTION_TYPE,O_STATE,	O_JURISDICTION_NAME,IBGE_Origin,O_SELLER_CONDITION,D_JURISDICTION_TYPE,D_STATE,D_JURISDICTION_NAME,IBGE_Destination,D_BUYER_CONDITION,
IMPOSITION,TAXRATE,BASIS_FACTOR,Reduced_Rate,TAX_RESPONSIBILITY,TAX_IN_FAVOR_OF,EFFDATE,ENDDATE,CITATION,Rules_Created_or_Updated_at,New_Changed_Indicator,Tax_Rule_ID,[HASH]) 
select CSE_CODI, RULE_DESCRIPTION, NCM_SERVICE_CODE, CESTCode, CEST_Description, MATERIAL_ORIGIN, TRANSACTION_TYPE, TRANSACTION_GEO, O_COUNTRY, O_JURISDICTION_TYPE,
O_STATE,O_JURISDICTION_NAME, IBGE_Origin, O_SELLER_CONDITION,D_JURISDICTION_TYPE, D_STATE,D_JURISDICTION_NAME,IBGE_Destination,D_BUYER_CONDITION,IMPOSITION,
TAXRATE,BASIS_FACTOR,Reduced_Rate,TAX_RESPONSIBILITY,TAX_IN_FAVOR_OF,EFFDATE,ENDDATE,CITATION,FORMAT(GETDATE(), 'dd/MMM/yyyy') as Rules_Created_or_Updated_at, 'X' as New_Changed_Indicator,
Tax_Rule_ID, hash_regra from #tmpRETORNO_ISS (nolock)

INSERT INTO tbl_vertex_regra_iss (CSE_CODI,RULE_DESCRIPTION,NCM_SERVICE_CODE,CESTCode,CEST_Description,MATERIAL_ORIGIN,TRANSACTION_TYPE,TRANSACTION_GEO,
O_COUNTRY,O_JURISDICTION_TYPE,O_STATE,	O_JURISDICTION_NAME,IBGE_Origin,O_SELLER_CONDITION,D_JURISDICTION_TYPE,D_STATE,D_JURISDICTION_NAME,IBGE_Destination,D_BUYER_CONDITION,
IMPOSITION,TAXRATE,BASIS_FACTOR,Reduced_Rate,TAX_RESPONSIBILITY,TAX_IN_FAVOR_OF,EFFDATE,ENDDATE,CITATION,Rules_Created_or_Updated_at,New_Changed_Indicator,Tax_Rule_ID,[HASH]) 
select CSE_CODI, RULE_DESCRIPTION, NCM_SERVICE_CODE, CESTCode, CEST_Description, MATERIAL_ORIGIN, TRANSACTION_TYPE, TRANSACTION_GEO, O_COUNTRY, O_JURISDICTION_TYPE,
O_STATE,O_JURISDICTION_NAME, IBGE_Origin, O_SELLER_CONDITION,D_JURISDICTION_TYPE, D_STATE,D_JURISDICTION_NAME,IBGE_Destination,D_BUYER_CONDITION,IMPOSITION,
TAXRATE,BASIS_FACTOR,Reduced_Rate,TAX_RESPONSIBILITY,TAX_IN_FAVOR_OF,EFFDATE,ENDDATE,CITATION,FORMAT(GETDATE(), 'dd/MMM/yyyy') as Rules_Created_or_Updated_at, 'X' as New_Changed_Indicator,
Tax_Rule_ID, hash_regra from #tmpRETORNO_ISS_duplicado (nolock)

SELECT TOP 1 NOMEARQUIVO Collate SQL_Latin1_General_CP1253_CI_AI FROM #tmpRETORNO_ISS

--SELECT DO TABEL�O PARA GERAR O CSV
select
	RULE_DESCRIPTION	Collate SQL_Latin1_General_CP1253_CI_AI	as 'RULE DESCRIPTION', 
	NCM_SERVICE_CODE	Collate SQL_Latin1_General_CP1253_CI_AI	as 'NCM OR SERVICE CODE', 
	CESTCode			Collate SQL_Latin1_General_CP1253_CI_AI	as 'CEST Code', 
	CEST_Description	Collate SQL_Latin1_General_CP1253_CI_AI	as 'CEST Description', 
	MATERIAL_ORIGIN		Collate SQL_Latin1_General_CP1253_CI_AI	as 'MATERIAL ORIGIN', 
	TRANSACTION_TYPE	Collate SQL_Latin1_General_CP1253_CI_AI	as 'TRANSACTION TYPE', 
	TRANSACTION_GEO		Collate SQL_Latin1_General_CP1253_CI_AI	as 'TRANSACTION GEO', 
	O_COUNTRY			Collate SQL_Latin1_General_CP1253_CI_AI	as 'O COUNTRY', 
	O_JURISDICTION_TYPE	Collate SQL_Latin1_General_CP1253_CI_AI	as 'O JURISDICTION TYPE',
	O_STATE				Collate SQL_Latin1_General_CP1253_CI_AI	as 'O STATE',
	O_JURISDICTION_NAME	Collate SQL_Latin1_General_CP1253_CI_AI	as 'O JURISDICTION NAME', 
	IBGE_Origin			Collate SQL_Latin1_General_CP1253_CI_AI	as 'IBGE_Origin', 
	O_SELLER_CONDITION	Collate SQL_Latin1_General_CP1253_CI_AI	as 'O SELLER CONDITION',
	D_JURISDICTION_TYPE	Collate SQL_Latin1_General_CP1253_CI_AI	as 'D JURISDICTION TYPE', 
	D_STATE				Collate SQL_Latin1_General_CP1253_CI_AI	as 'D STATE',
	D_JURISDICTION_NAME	Collate SQL_Latin1_General_CP1253_CI_AI	as 'D JURISDICTION NAME',
	IBGE_Destination	Collate SQL_Latin1_General_CP1253_CI_AI	as 'IBGE_Destination',
	D_BUYER_CONDITION	Collate SQL_Latin1_General_CP1253_CI_AI	as 'D BUYER CONDITION',
	IMPOSITION			Collate SQL_Latin1_General_CP1253_CI_AI	as 'IMPOSITION',
	TAXRATE				Collate SQL_Latin1_General_CP1253_CI_AI	as 'TAXRATE',
	BASIS_FACTOR		Collate SQL_Latin1_General_CP1253_CI_AI	as 'BASIS FACTOR',
	Reduced_Rate		Collate SQL_Latin1_General_CP1253_CI_AI	as 'TAX RESULT TYPE',
	TAX_RESPONSIBILITY	Collate SQL_Latin1_General_CP1253_CI_AI	as 'TAX RESPONSIBILITY',
	TAX_IN_FAVOR_OF		Collate SQL_Latin1_General_CP1253_CI_AI	as 'TAX IN FAVOR OF',
	EFFDATE				Collate SQL_Latin1_General_CP1253_CI_AI	as 'EFFDATE',
	ENDDATE				Collate SQL_Latin1_General_CP1253_CI_AI	as 'ENDDATE',
	CITATION			Collate SQL_Latin1_General_CP1253_CI_AI	as 'CITATION',
	Rules_Created_or_Updated_at as 'RULES CREATED OR UPDATED AT',
	'' as 'NEW CHANGED INDICATOR',
	Tax_Rule_ID as 'TAX RULE ID'
	from tbl_vertex_regra_iss
	ORDER BY CSE_CODI