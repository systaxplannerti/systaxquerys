use DB482265
--drop table #tmpRETORNO_ISS_RET;
--drop table #tmpUPDATE_ISS_RET;
--drop table #tmpRETORNO_ISS_duplicado;



select 
			CS.cse_codi AS  CSE_CODI,
			CAST('SERV: ' + RIGHT('0' + COD_SUBITEM_LEI, 5) + ' ISS ' + CAST(CAST(   REPLACE( REPLACE(ISS_ALIQ , ' ', '0')  ,',','.') AS DECIMAL(18,2)) AS VARCHAR(30)) + '%' AS VARCHAR(100))  as RULE_DESCRIPTION,
			--'SERV: ' + COD_SUBITEM_LEI + '_ISS_' + CAST(CAST(   REPLACE( REPLACE(ISS_ALIQ , ' ', '0')  ,',','.') AS DECIMAL(18,2)) AS VARCHAR(30)) + '%' as RULE_DESCRIPTION,
			COD_SUBITEM_LEI AS NCM_SERVICE_CODE,
			null AS CESTCode,
			null AS CEST_Description,
			'Domestic' AS MATERIAL_ORIGIN,

CASE 
	WHEN ISS_GENE IN ('Nao tem reten�ao')
		THEN 'Acquisition'
		ELSE null 
		END AS TRANSACTION_TYPE,
CASE
	WHEN ISS_GENE IN ('Nao tem reten�ao')
		THEN 'Acquisition'
		ELSE null 
		END AS TRANSACTION_GEO,

'BRAZIL' AS  O_COUNTRY,
'Country' AS  O_JURISDICTION_TYPE,
'All States' AS  O_STATE,
'BRAZIL' AS  O_JURISDICTION_NAME,
 null AS  IBGE_Origin,
'Origin - Service Provider' AS  O_SELLER_CONDITION,

'Country' AS  D_JURISDICTION_TYPE,
'All States' AS  D_STATE,
'BRAZIL' AS  D_JURISDICTION_NAME,

null AS  IBGE_Destination,
'Destina��o ' AS D_BUYER_CONDITION,
'ISS' AS  IMPOSITION, --?
CAST(CAST(   REPLACE( REPLACE(ISS_ALIQ , ' ', '0')  ,',','.') AS DECIMAL(18,2)) AS VARCHAR(30)) + '%' AS  TAXRATE, --?
CASE 
	WHEN ISS_GENE IN ('ISSRedBC')
		THEN SUBSTRING(ISS_GENE, CHARINDEX('=', ISS_GENE) + 1, LEN(ISS_GENE))
		ELSE '100,0000%' 
		END AS BASIS_FACTOR, 
CASE 
	WHEN ISS_GENE IN ('ISSAlqRed=1')
		THEN 'Reduced Rate'
		ELSE 'Taxable Standard'
		END AS  Reduced_Rate,
CASE 
	WHEN ISS_RESPONSAVEL = 'Prestador'
		THEN 'Seller'
		ELSE 'Buyer'
		END AS TAX_RESPONSIBILITY,

CASE 
	WHEN ISS_MUN_INCIDENCIA = 'Prestador'
		THEN 'Origin'
		ELSE 'Destination'
		END AS TAX_IN_FAVOR_OF,
'Taxable Standard' AS [TAX RESULT TYPE],
'Buyer' AS [TAX RESPONSIBILITY],
'Origin' AS [TAX IN FAVOR OF],
'' AS [EFFDATE], --?
'99991231' AS  ENDDATE,

ISS_DISPLEG AS  CITATION, --?
null AS  Rules_Created_or_Updated_at,
null AS  New_Changed_Indicator,
null AS  Tax_Rule_ID,
HASHBYTES('MD5', 
'SERV' + isnull(COD_SUBITEM_LEI,'') + 'ISS' + isnull(ISS_ALIQ,'') + 'from:' + isnull(MUNICIPIO_PRESTADOR,'') + 'to:' + isnull(ID_MUNICIPIO_SERVICO,'')
+ COD_SUBITEM_LEI 
+ 'Domestic'
+ CASE 
	WHEN ISS_GENE IN ('Nao tem reten�ao')
		THEN 'Acquisition'
		ELSE null 
		END 
+ CASE
	WHEN ISS_GENE IN ('Nao tem reten�ao')
		THEN 'Acquisition'
		ELSE null 
		END
+ 'BRAZIL' 
+ 'Country' 
+ 'All States' 
+ 'BRAZIL' 
+ 'Origin - Service Provider'




) as hash_regra  
into #tmpRETORNO_ISS_RET
 from tbl_cenario_servico cs (nolock)
inner join tbl_servico s (nolock) on cs.ser_codi = s.ser_codi
inner join tbl_cenario c (nolock) on cs.cenario_codi = c.cenario_codi
inner join tbl_imp_iss imp (nolock) on cs.cse_codi = imp.cse_codi and imp.imp_ativ = 1 and IMP_STATUS = 'A'
inner join tbl_iss iss (nolock) on imp.imp_codi = iss.imp_codi
LEFT join TBL_IMP_RETENCAO IR (nolock) on CS.CSE_CODI = IR.CSE_CODI AND CS.IMP_RETENCAO = IR.IMP_CODI
LEFT JOIN TBL_RETENCAO RET (NOLOCK) ON IR.IMP_CODI = RET.IMP_CODI
where cs.cse_ativ = 1

--ALTERANDO RULE_DESCRIPTION COM O FROM E TO QUE FOI MAPEADO
update #tmpRETORNO_ISS_RET set RULE_DESCRIPTION = RULE_DESCRIPTION + ' from ' + O_SELLER_CONDITION + ' to ' + D_BUYER_CONDITION

--BUSCANDO REGRAS DE ISS QUE J� EXISTIA E CONTINUA IGUAL PARA ATUALIZAR O INDICADOR E RETIRAR DA TABELA DE APOIO
select VERTEX_REGRA_ISS_CODI, A.CSE_CODI into #tmpUPDATE_ISS_RET from tbl_vertex_regra_iss a (nolock)
inner join #tmpRETORNO_ISS_RET b (nolock) on a.cse_codi = b.cse_codi
where a.[hash] = b.hash_regra
--ATUALIZANDO INDICADOR DA REGRA QUE J� EXISTIA 
update u set u.New_Changed_Indicator = null from #tmpUPDATE_ISS_RET t
inner join tbl_vertex_regra_iss u on t.VERTEX_REGRA_ISS_CODI = u.VERTEX_REGRA_ISS_CODI
--EXCLUINDO REGRA ISS QUE J� EXISTE E EST� IGUAL AO TABEL�O DE RETORNO
delete b from #tmpUPDATE_ISS_RET a
inner join #tmpRETORNO_ISS_RET b on a.CSE_CODI = b.CSE_CODI

--EXCLUINDO REGRA ISS DO TABEL�O, POIS A ATUAL � DIFERENTE
delete a from tbl_vertex_regra_iss a
inner join #tmpRETORNO_ISS_RET b on a.cse_codi = b.cse_codi
where a.[hash] != b.hash_regra

--INSERINDO RESTANTE NO TABEL�O DE RETORNO
INSERT INTO tbl_vertex_regra_iss (CSE_CODI,RULE_DESCRIPTION,NCM_SERVICE_CODE,CESTCode,CEST_Description,MATERIAL_ORIGIN,TRANSACTION_TYPE,TRANSACTION_GEO,
O_COUNTRY,O_JURISDICTION_TYPE,O_STATE,	O_JURISDICTION_NAME,IBGE_Origin,O_SELLER_CONDITION,D_JURISDICTION_TYPE,D_STATE,D_JURISDICTION_NAME,IBGE_Destination,D_BUYER_CONDITION,
IMPOSITION,TAXRATE,BASIS_FACTOR,Reduced_Rate,TAX_RESPONSIBILITY,TAX_IN_FAVOR_OF,EFFDATE,ENDDATE,CITATION,Rules_Created_or_Updated_at,New_Changed_Indicator,Tax_Rule_ID,[HASH]) 
select CSE_CODI, RULE_DESCRIPTION, NCM_SERVICE_CODE, CESTCode, CEST_Description, MATERIAL_ORIGIN, TRANSACTION_TYPE, TRANSACTION_GEO, O_COUNTRY, O_JURISDICTION_TYPE,
O_STATE,O_JURISDICTION_NAME, IBGE_Origin, O_SELLER_CONDITION,D_JURISDICTION_TYPE, D_STATE,D_JURISDICTION_NAME,IBGE_Destination,D_BUYER_CONDITION,IMPOSITION,
TAXRATE,BASIS_FACTOR,Reduced_Rate,TAX_RESPONSIBILITY,TAX_IN_FAVOR_OF,EFFDATE,ENDDATE,CITATION,FORMAT(GETDATE(), 'yyyyMMdd') as Rules_Created_or_Updated_at, 'X' as New_Changed_Indicator,
Tax_Rule_ID, hash_regra from #tmpRETORNO_ISS_RET (nolock)

--SELECT DO TABEL�O PARA GERAR O CSV
select RULE_DESCRIPTION, NCM_SERVICE_CODE, CESTCode, CEST_Description, MATERIAL_ORIGIN, TRANSACTION_TYPE, TRANSACTION_GEO, O_COUNTRY, O_JURISDICTION_TYPE,
O_STATE,O_JURISDICTION_NAME, IBGE_Origin, O_SELLER_CONDITION,D_JURISDICTION_TYPE, D_STATE,D_JURISDICTION_NAME,IBGE_Destination,D_BUYER_CONDITION,IMPOSITION,
TAXRATE,BASIS_FACTOR,Reduced_Rate,TAX_RESPONSIBILITY,TAX_IN_FAVOR_OF,EFFDATE,ENDDATE,CITATION,Rules_Created_or_Updated_at,New_Changed_Indicator,
Tax_Rule_ID from tbl_vertex_regra_iss